<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Categorias'=>array('index'),
		'Crear',
	),
));

?>

<h1>Crear categoria</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>