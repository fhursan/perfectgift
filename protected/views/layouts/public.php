<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
	<meta charset="utf-8" />
	<meta name="description" content="Encuentre su regalo para cada ocasion especial. Aqui tenemos el regalo eprfecto.">
	<meta name="keywords" content="Regalo perfecto, Tartas de pañales, ramos de ropa, ramos de flores, canastillas, cestas de regalo ">
	<meta name="viewport" content="width=device-width">

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	
	<?php Yii::app()->clientScript->registerScriptFile('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js', CClientScript::POS_HEAD); ?>
	<?php Yii::app()->clientScript->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', CClientScript::POS_HEAD); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.flexslider-min.js', CClientScript::POS_HEAD); ?>
	<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/retina.js', CClientScript::POS_HEAD); ?>

	
	<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/public.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="page">

	<div id="content">
		
		<?php $this->renderPartial("/layouts/_menu"); ?>
		
		<div class="loader"><img src="<?php echo Yii::app()->baseURL; ?>/images/loader.gif" /></div>
		
		<?php echo $content; ?>
		
		<div style="clear:both;"></div>
		
	</div><!-- content -->

</div><!-- page -->

<?php $this->renderPartial("/layouts/_footer"); ?>

</body>
</html>

<script>
	$(".home").hide();
	
	$(document).ready(function(){
		$(".help").tooltip({ position: { my: "left+10 center", at: "right center" } });
	});
</script>