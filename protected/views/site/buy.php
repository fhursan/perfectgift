<div class="home">

	<?php 
		$this->breadcrumbs=array(
			"Comprar: " . $model->name,
		);
	?>
		
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'homeLink' => CHtml::link('Inicio', Yii::app()->homeUrl),
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<div id="productData">
	
		<h1>Estas a punto de comprar: <span style="color: #333; font-weight: normal;"><? echo $model->name; ?></span></h1>
	
		<div class="productPhoto">
			<img src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => Product::model()->getPhoto($model->ID))); ?>" style="width: 100%;" />
		</div><!-- productPhoto -->
		
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'buy-form',
			'action'=>array("buy", "id"=>$model->ID),
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		)); ?>
		
			<div class="productText">
				<p><? echo Functions::stringCut($model->description, 80); ?></p>
				<div style="float:right;">
					<p>
						Precio: <br />
						<span style="font-size: 1.4em;"><span id="priceFinal"><?php echo str_replace('.',',',number_format($model->price, 2)); ?></span><span class="euro">&euro;</span></span><br />
						<span class="little">IVA y transporte incluido</span>
					</p>
				</div>
				
				<div style="float:left; width: 40%;">
					<p>
						Unidades a enviar: <br />
						<span id="addUnit">+</span> 
							<input type="hidden" id="price" name="price" value="<?php echo $model->price; ?>" />
							<input type="hidden" name="product" id="product" value="<?php echo $model->ID; ?>" /> 
							<input type="text" id="unit" name="unit" style="width: 25%; text-align: center;" value="1" readonly /> 
						<span id="quitUnit">-</span>
					</p>
				</div>
			</div><!-- productText -->
	
			<div style="clear: both;"></div><br /><br />
			
			<?php echo $modelCustomer ? $form->errorSummary($modelCustomer) : ""; ?>
			<?php echo $modelOrder ? $form->errorSummary($modelOrder) : ""; ?>
		
			<h1>Datos de destinatario</h1>
						
			<div style="float:left; width: 45%;" class="orderSummary">
				<label for="name" class="help" title="Hemos calculado la fecha de entrega para el primer d&iacute;a disponible. Si desea que se entregue m&aacute;s tarde puede cambiarla.">
					* Fecha de entrega
				</label>
				<?php
					// Double if line first if friday add two days, else if late than 6pm add one day.
					if(date("N") == 5) :
						$defaultDate = date("d-m-Y", time()+ (86400 * 3));
					elseif(date("H") < 18) :
						$defaultDate = date("d-m-Y", time()+86400);
					else:
						$defaultDate = date("d-m-Y", time()+172800);
					endif;

		            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
		              'name'=>'date',
		              'value'=>$defaultDate,
		              // additional javascript options for the date picker plugin
		              'options'=>array(
		                'changeYear'=>true,
		                'yearRange'=>'2010:2050',
		                'language'=>'es',
		                'dateFormat'=>'dd-mm-yy',
		                'defaultDate'=>$defaultDate,
		                'monthNames' => array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"),
		                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
		                'dayNames' => array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"),
		                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
		                'firstDay'=>1,
		              ),
		              'htmlOptions'=>array(
		                'placeholder'=>'dd-mm-aaaa',
		                'style'=>'width:80%;',
		              ),
		            ));
		        ?><br />
		        
				<label for="name">* Nombre</label>
				<input type="text" name="name" id="name" style="width: 100%;" maxlength="100" />
				
				<label for="surname">* Apellidos</label>
				<input type="text" name="surname" id="surname" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="float:right; width: 45%;">
				<div style="border: 1px solid #E4B2CC; padding: 5%;">
					<p style="text-align: left; margin: 0;">Su pedido se <b>entregar&aacute; en 24h</b> a no ser que sean <b>m&aacute;s de las 18:00h</b>. Tampoco se hacen entregas los <b>domingos o festivos</b>.<br /><br /> Gracias</p>
				</div>
			</div>
			
			<div style="clear: both;"></div>
			
			<div class="orderSummary">
				<label for="address">* Direccion de entrega</label>
				<input type="text" name="address" id="address" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="float:left; width: 45%;" class="orderSummary">
				<label for="name" class="help" title="Se solicita solo a nivel informativo. No se enviara ningun aviso.">
					* Email
				</label>
				<input type="text" name="email" id="email" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="float:right; width: 45%;" class="orderSummary">
				<label for="surname" class="help" title="Se solicita solo a nivel informativo. No se enviara ningun aviso.">
					* Telefono
				</label>
				<input type="text" name="phone" id="phone" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="clear: both;"></div>
			
			<div class="orderSummary">
				<label for="name" class="help" title="Puede escribir una dedicatoria y adjuntar una foto para incluirla en el regalo impresa en una tarjeta personalizada.">
					Dedicatoria
				</label>
				<textarea name="comment" id="comment" style="width: 100%;" maxlength="100"></textarea>
			</div>
			
			<div id="preview"><img id="thephoto" style="display:none; width: 200px; height: 150px;" /></div>
			
			<div style="float:left; width: 100%;" class="orderSummary">
				Si lo desea puede a&ntilde;adir una imagen a su dedicatoria. Seleccione la imagen haciendo click en Examinar<br />
				<input type="file" name="photo" id="photo" style="width: 100%;" maxlength="200" rows="6" />
			</div>
			
			<div style="clear: both;"></div><br /><br />
			
			<h1>Datos del remitente</h1>
			
			<div style="float:left; width: 45%;" class="orderSummary">
				<label for="rname">* Nombre remitente</label>
				<input type="text" name="rname" id="rname" style="width: 100%;" maxlength="100" />
				
				<label for="rsurname">* Apellidos remitente</label>
				<input type="text" name="rsurname" id="rsurname" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="float:right; width: 45%;" class="orderSummary">
				<label for="DNI">* DNI remitente</label>
				<input type="text" name="DNI" id="DNI" style="width: 100%;" maxlength="100" />
				
				<label for="remail">* Email remitente</label>
				<input type="text" name="remail" id="remail" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="clear: both;"></div>
			
			<div class="orderSummary">
				<label for="address">* Direccion del remitente</label>
				<input type="text" name="raddress" id="raddress" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="clear: both;"></div>
			
			<div style="float:left; width: 45%;" class="orderSummary">
				<label for="rphone">* Telefono remitente</label>
				<input type="text" name="rphone" id="rphone" style="width: 100%;" maxlength="100" />
			</div>
			
			<div style="clear: both;"></div>
			
			<div style="float:left; width: 45%;">
				<input type="checkbox" name="lopd" id="lopd" /> Acepto las condiciones de uso
			</div>
			
			<div style="clear: both;"></div>
			
			<div style="margin-top: 5%;">
				<input type="button" id="submitForm" value="Siguiente paso" />
			</div>
			
			
			<div style="clear: both;"></div><br /><br />

		
		<?php $this->endWidget(); ?>
		
	</div><!-- productData -->
	
</div><!-- home -->

<script>
	$(document).ready(function(){		
		$("#addUnit").click(function(){
			if($("#unit").val() != 25)
			{
				$("#unit").val(parseInt($("#unit").val()) + 1);
				var price;
				price = (((parseFloat($("#price").val()) * parseFloat($("#unit").val())) *100) / 100);
				$("#priceFinal").html( price.toString().replace(".",",") );	
			}
		});
		
		$("#quitUnit").click(function(){
			if($("#unit").val() != 1)
			{
				$("#unit").val(parseInt($("#unit").val()) - 1);
				var price;
				price = (((parseFloat($("#price").val()) * parseFloat($("#unit").val())) *100) / 100);
				$("#priceFinal").html( price.toString().replace(".",",") );	
			}
		});
		
		$("#photo").change(function(){
			oFReader = new FileReader();
	        
	        oFReader.readAsDataURL(this.files[0]);
	
	        oFReader.onload = function (oFREvent) {
	            document.getElementById("thephoto").src = oFREvent.target.result;
	            $("#thephoto").css({"display" : "block"});
	        };
		});
		
		$("#submitForm").click(function(){
			var error = "";
			
			var year = parseInt($("#date").val().substring(6, 10));
			var month = parseInt($("#date").val().substring(3, 5) - 1);
			var day = parseInt($("#date").val().substring(0, 2));
			
			var date = new Date(year, month, day); 
			var today = new Date();
			
			console.log("date: " + date.getTime() + ", " + date);
			console.log("today: " + today.getTime() + ", " + today);
			
			if(date.getTime() <= today.getTime()){ error = error + "- La fecha de entrega debe ser posterior\n\r"; $("#date").addClass("error");  } else { $("#date").removeClass("error"); }
						
			if($("#date").val() == ""){ error = error + "- Debe de poner una fecha de entrega\n\r"; $("#date").addClass("error"); } else { $("#date").removeClass("error"); }
			
			if($("#name").val() == ""){ error = error + "- Debe de poner el nombre del destinatario\n\r"; $("#name").addClass("error"); } else { $("#name").removeClass("error"); }
			if($("#surname").val() == ""){ error = error + "- Debe de poner los apellidos del destinatario\n\r"; $("#surname").addClass("error"); } else { $("#surname").removeClass("error"); }
			if($("#address").val() == ""){ error = error + "- Debe de poner la direccion de entrega del destinatario\n\r"; $("#address").addClass("error"); } else { $("#address").removeClass("error"); }
			if($("#email").val() == ""){ error = error + "- Debe de poner el email del destinatario\n\r"; $("#email").addClass("error"); } else { $("#email").removeClass("error"); }
			if($("#phone").val() == ""){ error = error + "- Debe de poner el telefono del destinatario\n\r"; $("#phone").addClass("error"); } else { $("#phone").removeClass("error"); }
			if($("#rname").val() == ""){ error = error + "- Debe de poner el nombre del remitente\n\r"; $("#rname").addClass("error"); } else { $("#rname").removeClass("error"); }
			if($("#rsurname").val() == ""){ error = error + "- Debe de poner los apellidos del remitente\n\r"; $("#rsurname").addClass("error"); } else { $("#rsurname").removeClass("error"); }
			if($("#DNI").val() == ""){ error = error + "- Debe de poner el DNI del remitente\n\r"; $("#DNI").addClass("error"); } else { $("#DNI").removeClass("error"); }
			if($("#raddress").val() == ""){ error = error + "- Debe de poner la direccion del remitente\n\r"; $("#raddress").addClass("error"); } else { $("#raddress").removeClass("error"); }
			if($("#remail").val() == ""){ error = error + "- Debe de poner el email del remitente\n\r"; $("#remail").addClass("error"); } else { $("#remail").removeClass("error"); }
			if($("#rphone").val() == ""){ error = error + "- Debe de poner el telefono del remitente\n\r"; $("#rphone").addClass("error"); } else { $("#rphone").removeClass("error"); }
			if(!$('#lopd').prop('checked'))	{ error = error + "- Debe aceptar las condiciones de uso\n\r"; }
			
			if(error != "")
			{
				alert(error)
;			}
			else
			{
				$("#buy-form").submit();
			}
		});
		
		$(".loader").hide();
		$(".home").fadeIn();
	});
</script>
