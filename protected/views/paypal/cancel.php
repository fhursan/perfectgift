<div class="home">

	<h3>Pago cancelado</h3>
	<p>
		Usted ha decidido cancelar el pago, por tanto el pedido queda anulado.<br /><br />
		Gecias por su tiempo, y si tiene alguna duda puede ponerse en contacto con nosotros a trav&eacute;s de <a href="mailto: info@unregaloperfecto.es">info@unregaloperfecto.es</a>
	</p>

</div>

<script>
	$(window).load(function(){
		$(".loader").hide();
		$(".home").fadeIn();
	});
</script>