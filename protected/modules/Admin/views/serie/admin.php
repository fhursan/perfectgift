<?php
/* @var $this SerieController */
/* @var $model Serie */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Series'=>array('index'),
		'Listado',
	),
));
?>

<h1>Gestion de series</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'serie-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'template'=>'{items} {summary} {pager}',
	'columns'=>array(
		array(
            'name'=>'serie',
            'value'=>'$data->serie',
            'headerHtmlOptions'=>array(
                'style'=>'width:75%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'default',
            'value'=>'$data->default == 0 ? "No" : "Si";',
            'headerHtmlOptions'=>array(
                'style'=>'width:10%;text-align:left !important;',
            ),
        ),
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete}',
			'buttons'=>array(
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
