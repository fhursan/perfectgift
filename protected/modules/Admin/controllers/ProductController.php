<?php

class ProductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$Admin = "isset(Yii::app()->user->role) && (Yii::app()->user->role==='admin')";
		$User  = "isset(Yii::app()->user->role) && (Yii::app()->user->role==='user')";
            
        return array(
        	array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('upload'),
				'users'=>array('*'),
			),
        	array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
                'expression'=>$User,
			),
            
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','admin','index','galleria', 'deletephoto'),
				'users'=>array('@'),
                'expression'=>$Admin,
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Product;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			$model->price = str_replace(",",".",$model->price);
			if($model->save())
				$this->redirect(array('update','id'=>$model->ID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$modelPhotos=Photo::model()->findAllByAttributes(array("productID"=>$model->ID));
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Product']))
		{
			$model->attributes=$_POST['Product'];
			$model->price = str_replace(",",".",$model->price);
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
			'modelPhotos'=>$modelPhotos,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Product('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Product']))
			$model->attributes=$_GET['Product'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Product the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Product::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Product $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/**
	 * Upload photos to server and relate them to product
	 */
	public function actionUpload()
	{
		$images = CUploadedFile::getInstancesByName('myPicture');
        if (isset($images) && count($images) > 0) {
            foreach ($images as $image => $pic) {
                $imagen = trim(rand(0,100000)."_".$pic->name);
                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/images/userfiles/'.$imagen)) {
                	
                	Yii::import('application.extensions.image.Image');
                	
                	$img = New Image(Yii::app()->params->imgPath.$imagen);
	    
				    if($img->width > $img->height) :
				    	$img->resize(800, 600, Image::WIDTH)->quality(70);
				    else :
				    	$img->resize(600, 450, Image::HEIGHT)->quality(70);
				    endif;
				    
				    $img->save();
				    
                	$photo = new Photo;
                	$photo->path = $imagen;
                	$photo->productID = $_GET["id"];
                	
                	print_r($photo->getErrors());
                	
                	$photo->save();
	                
	                echo 1;
                }
                else
                {
                    echo "<script>console.log('error!');</script>";
                }
            }

        }
        else
        {
	        echo "No hay ficheros";
        }
	}
	
	/**
	 * Delete photo to server and relate them to product
	 */
	public function actionDeletephoto()
	{
		$modelPhoto = Photo::model()->findByPK($_POST["id"]);
				
		$modelPhoto->delete();
	}
	
	/**
	 * Render partial of _photo with the photo gallery.
	 */
	 public function actionGalleria()
	 {
	 	$model=$this->loadModel($_GET["id"]);
	 	$modelPhotos=Photo::model()->findAllByAttributes(array("productID"=>$_GET["id"]));
	 	$this->renderPartial("_photo", array('model'=>$model,'modelPhotos'=>$modelPhotos), false, true);
	 }
}
