<h2>Fotos del producto</h2>

<?php if($model->isNewRecord): ?>
	<p class="nofield">Antes de poder subir fotos tienes que crear el producto. Rellena el formulario y pulsa Guardar, entonces podrás subir las fotos.</p>
<?php else: ?>
	<p class="nofield" style="color: red;">La medida recomendada es 800x600 para que las fotos no se vean cortadas o achatadas.</p>
	<div class="multiselect">
		<?php if(count(Photo::model()->findAllByAttributes(array("productID"=>$model->ID)))>0): ?>
			<?php $this->beginWidget('application.extensions.galleria.Galleria', array(
									'options' => array(//galleria options
										'transition' => 'fade',
										'debug' => true,
										'thumbnails' => true,
										'trueFullscreen' => true,
										'fullscreenDoubleTap' => true,
									)
			));?>
			
				<?php foreach($modelPhotos as $photo): ?>
					<img src="<?php echo Yii::app()->baseURL.Yii::app()->params->imgPublicPath.$photo->path; ?>" />
				<?php endforeach; ?>
			<?php $this->endWidget();?><br />
		<?php else: ?>
			<p class='nofield'>Este producto <b>no tiene</b> fotos todav&iacute;a. Insertalas haciendo click en el bot&oacute;n examinar.</p>
		<?php endif; ?>
		
		<?php
			$this->widget('ext.uploadify.MUploadify',array(
				'name'=>'myPicture',
				'buttonText'=>Yii::t('application','Examinar'),
				'multi'=>true,
				'auto'=>true,
				'debug'=>true,
				'script'=>array('product/upload','id'=>$model->ID),
				//'checkScript'=>array('myController/checkUpload','id'=>$model->id),
				'fileExt'=>'*.jpg;*.jpeg;*.png;',
				'folder'=>Yii::getPathOfAlias('webroot').'/images/userfiles/',
				//fileDesc=>Yii::t('application','Image files'),
				'uploadButton'=>false,
				'uploadButtonText'=>'Subir imagenes',
				'onError' => 'js:function(evt,queueId,fileObj,errorObj){alert("Error: " + errorObj.type + "\nInfo: " + errorObj.info);}',
				//'uploadButtonTagname'=>'button',
				//'uploadButtonOptions'=>array('class'=>'myButton'),
				'onAllComplete'=>'js:function(){
												$("#galeria").html("Recargando la galer&iacute;a...");
												var request = $.ajax({
									  				url: "'.$this->createURL("/Admin/product/galleria", array("id"=>$model->ID)).'",
											  		type: "POST",
									  				dataType: "html"
												});
									
												request.done(function(msg) {
									  				$("#galeria").html(msg);
									  				$("#galeria").delay(300).fadeIn(500);
												});
										
												request.fail(function(jqXHR, textStatus) {
									  				console.log( "Error: " + textStatus );
												});
				}',
			));
		?><br /><br />
		
		<?php if(count(Photo::model()->findAllByAttributes(array("productID"=>$model->ID)))>0): ?>
			<h2>Eliminar fotos del producto</h2>
			<p>Haz click sobre la foto que desees eliminar.</p>
			
			<?php foreach($modelPhotos as $photo): ?>
					<img src="<?php echo Yii::app()->baseURL.Yii::app()->params->imgPublicPath.$photo->path; ?>" class="deletePhoto" id="<?php echo $photo->ID ?>" width="50" height="50" title="Haz click en la foto para borrarla"/>
			<?php endforeach; ?><br />
		<?php endif; ?>
	</div>
<?php endif; ?>

<script>
	/*
	 * Delete photo from photo list
	 */
	$(".deletePhoto").click(function(){
        if(confirm("Haz click en Aceptar si realmente quieres borrar esta foto.")){
	        $("#galeria").html("Recargando la galer&iacute;a...");
	        
	        var request = $.ajax({
  				url: "<?php echo Yii::app()->createURL("/Admin/product/deletephoto"); ?>",
		  		type: "POST",
  				data: {
  					id : $(this).attr("id"), 
  				},
  				dataType: "html"
			});

			request.done(function(msg) {
  				location.href = '<?php echo Yii::app()->createURL("/Admin/product/update", array("id"=>$model->ID)); ?>';
			});
	
			request.fail(function(jqXHR, textStatus) {
  				alert( "Error: " + textStatus );
			});
		}
    });
</script>