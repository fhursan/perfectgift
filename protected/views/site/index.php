<div class="home">
	
	<div id="contentSliderHome" class="flexslider">
		
		<div id="caption">
			
			<a href="#"><span id="slideName">Product name</span></a>
		
			<a href="#"><span id="slideBuy">COMPRAR</span></a>
		
		</div>
		
		<ul class="slides">
			
			<?php 
				$criteria=new CDbCriteria;
				$criteria->condition = "promo = :promo";
				$criteria->params = array(':promo' => 1);
				
				$modelProducts = Product::model()->findAll($criteria); 
			?>
			
			<?php foreach($modelProducts as $product): ?>
			
				<?php $modelPhoto = Photo::model()->findByAttributes(array('productID' => $product->ID), array('order' => 'ID desc')); ?>
			
				<li>
					<a href="<?php echo Yii::app()->createURL('site/product', array('id'=>$product->ID)); ?>">	
						<img id="<?php echo $product->ID; ?>" src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => $modelPhoto->path)); ?>" alt="<?php echo $product->name; ?> / <?php echo str_replace('.',',',number_format($product->price, 2)); ?>&euro;"  style="max-width: 100%;" />
					</a>
				</li>
			
			<?php endforeach; ?>
			
		</ul><!-- slides -->
		
	</div><!-- contentSlider -->
	
	<div id="sections">
		<?php $index = 0; ?>
		
		<?php foreach(Category::model()->findAll(array('order' => 'name asc')) as $category) : ?>
			
			<?php $left = $index % 2 == 0 ? "left" : ""; // Odd or even ?>
			
			<div class="section <?php echo $left; ?>">
				<div id="caption">
				
					<a href="<?php echo Yii::app()->createURL('site/category', array('id'=>$category->ID)); ?>" class=clear" >
						<span id="minislideName"><?php echo mb_strtoupper($category->name,'utf-8'); ?></span>
					</a>
				
				</div>
				
				<a href="<?php echo Yii::app()->createURL('site/category', array('id'=>$category->ID)); ?>" class=clear" >
					<img src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => $category->photo)); ?>" />
				</a>
			</div><!-- section -->
			
			<?php $index++; ?>

		<?php endforeach; ?>
		
		<div style="clear:both;"></div>
	</div><!-- sections -->
	
</div><!-- home -->

<script>
	$(".home").hide();

	$(window).load(function(){		
		$('.flexslider').flexslider({
	    	animation: "slide",
	    	slideshowSpeed: 15000,
	    	controlNav: false,
	    	startAt: -1,
	    	
	    	start: function(){
		    	$("#slideName").html($('li.flex-active-slide img').attr('alt'));
		    	$("#slideName").parent().attr('id', $('li.flex-active-slide img').attr('id'));
		    	$("#slideBuy").parent().attr('id', $('li.flex-active-slide img').attr('id'));
	    	},
	    	
	    	after: function(){
		    	$("#slideName").html($('li.flex-active-slide img').attr('alt'));
		    	$("#slideName").parent().attr('id', $('li.flex-active-slide img').attr('id'));
		    	$("#slideBuy").parent().attr('id', $('li.flex-active-slide img').attr('id'));
	    	},
	    });
	    
	    $('#slideName').click(function(){
		    window.location.href = '<?php echo Yii::app()->createURL('site/product'); ?>/id/' + $(this).parent().attr('id');
	    });
	    
	    $('#slideBuy').click(function(){
		    window.location.href = '<?php echo Yii::app()->createURL('site/buy'); ?>/id/' + $(this).parent().attr('id');
	    });
	    
	    $(".loader").hide();
		$(".home").fadeIn();
	});
</script>
