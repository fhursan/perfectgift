<?php
/* @var $this OrderController */
/* @var $model Order */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Pedidos'=>array('admin'),
		'Listado',
	),
));

?>


<h1>Gesti&oacute;n de pedidos</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'order-grid',
	'dataProvider'=>$model->search(),
	'template'=>'{items} {summary} {pager}',
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'Producto',
            'filter'=>false,
            'value'=>'$data->product->name',
            'headerHtmlOptions'=>array(
                'style'=>'width:20%;text-align:left !important;',
            ),
        ),
		array(
            'name'=>'name',
            'filter'=>false,
            'value'=>'$data->name . " " . $data->surname',
            'headerHtmlOptions'=>array(
                'style'=>'width:20%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'email',
            'value'=>'$data->email',
            'headerHtmlOptions'=>array(
                'style'=>'width:20%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'phone',
            'filter'=>false,
            'value'=>'$data->phone',
            'headerHtmlOptions'=>array(
                'style'=>'width:10%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'status',
            'filter'=>false,
            'type'=>'html',
            'value'=>'Order::model()->status($data->status)',
            'headerHtmlOptions'=>array(
                'style'=>'width:10%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:10%;text-align:center !important;',
            ),
        ),
        array(
            'name'=>'price',
            'filter'=>false,
            'value'=>'$data->price . "€"',
            'headerHtmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
        ),
        array(
            'name'=>'units',
            'filter'=>false,
            'value'=>'$data->units',
            'headerHtmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
        ),
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete} {paypal}',
			'buttons'=>array(
				'paypal' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/paypal.png',
            		'label'=>Yii::t('admin', 'Ir a Paypal'),
            		//'url'=>Yii::app()->controller->createUrl('goal/admin'),
        		),
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
