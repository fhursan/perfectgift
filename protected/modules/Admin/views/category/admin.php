<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Categorias'=>array('index'),
		'Listado',
	),
));

?>

<h1>Gesti&oacute;n de categorias</h1>

<a href="<?php echo Yii::app()->createURL("/Admin/category/create"); ?>" class="blueButton">CREAR CATEGORIA</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'name',
            'value'=>'$data->name',
            'headerHtmlOptions'=>array(
                'style'=>'width:45%;text-align:left !important;',
            ),
        ),
		array(
            'name'=>'photo',
            'type' => 'html',
            'value'=>'(!empty($data->photo)) ? CHtml::image(Yii::app()->createURL("site/renderimagemini", array("path" => $data->photo))) : CHtml::image(Yii::app()->createURL("site/renderimagemini", array("path" => "nophoto.png"))) ',
        ),
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete}',
			'buttons'=>array(
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
