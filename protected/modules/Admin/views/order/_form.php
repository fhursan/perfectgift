<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'customerID'); ?>
	<?php echo $form->hiddenField($model,'productID'); ?>
	
	<div class="row" style="width:20%; float:left;">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date',array('style'=>'width: 80%;')); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
	
	<div class="row" style="width:10%; float:left;">
		<?php echo $form->labelEx($model,'units'); ?>
		<?php echo $form->textField($model,'units',array('style'=>'width: 30%;','maxlength'=>11)); ?>
		<?php echo $form->error($model,'units'); ?>
	</div>

	<div class="row" style="width:10%; float:left;">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('style'=>'width: 60%;','maxlength'=>10)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row" style="width:30%; float:left;">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', array(0 => "En proceso", 1 => "Pagado", 2 => "Enviado y facturado", 3 => "Entregado"), 
												array('empty'=>'-- Selecciona el estado --', 'style' => 'width: 50%;')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	
	<div style="clear:both;"></div><br /><br />
	
	<div style="border: 1px solid #dcdcdc; padding: 2%;">

		<div class="row" style="width:45%;float:left;">
			
			<h2>Datos del destinatario</h2>
			
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('style'=>'width: 40%;','maxlength'=>100)); ?>
			<?php echo $form->error($model,'name'); ?>
	
			<?php echo $form->labelEx($model,'surname'); ?>
			<?php echo $form->textField($model,'surname',array('style'=>'width: 40%;','maxlength'=>150)); ?>
			<?php echo $form->error($model,'surname'); ?>
			
			<?php echo $form->labelEx($model,'address'); ?>
			<?php echo $form->textField($model,'address',array('style'=>'width: 100%;','maxlength'=>250)); ?>
			<?php echo $form->error($model,'address'); ?>
			
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('style'=>'width: 100%;','maxlength'=>150)); ?>
			<?php echo $form->error($model,'email'); ?>
			
			<?php echo $form->labelEx($model,'phone'); ?>
			<?php echo $form->textField($model,'phone',array('style'=>'width: 20%;','maxlength'=>20)); ?>
			<?php echo $form->error($model,'phone'); ?>
		</div>
		
		<div class="row" style="width:45%;float:right;">
		
			<h2>Datos del remitente</h2>
		
			<?php echo $form->labelEx($model,'rname'); ?>
			<?php echo $form->textField($model,'rname',array('style'=>'width: 40%;','maxlength'=>100)); ?>
			<?php echo $form->error($model,'rname'); ?>
			
			<?php echo $form->labelEx($model,'rsurname'); ?>
			<?php echo $form->textField($model,'rsurname',array('style'=>'width: 40%;','maxlength'=>150)); ?>
			<?php echo $form->error($model,'rsurname'); ?>
			
			<?php echo $form->labelEx($model,'DNI'); ?>
			<?php echo $form->textField($model,'DNI',array('style'=>'width: 20%;','maxlength'=>12)); ?>
			<?php echo $form->error($model,'DNI'); ?>
			
			<?php echo $form->labelEx($model,'remail'); ?>
			<?php echo $form->textField($model,'remail',array('style'=>'width: 100%;','maxlength'=>150)); ?>
			<?php echo $form->error($model,'remail'); ?>
			
			<?php echo $form->labelEx($model,'rphone'); ?>
			<?php echo $form->textField($model,'rphone',array('style'=>'width: 20%;','maxlength'=>20)); ?>
			<?php echo $form->error($model,'rphone'); ?>
		
		</div>
		
		<div style="clear:both;"></div>
	
	</div><br /><br />
	
	<div style="border: 1px solid #dedede; padding: 2%;">
		<h2>Dedicatoria</h2>
		
		<div style="clear:both;"></div>
		
		<div class="row" style="width:30%;float:left;">
			<?php echo $form->hiddenField($model,'photo',array('size'=>60,'maxlength'=>150)); ?>
			<?php if($model->photo != ""): ?>
				<img src="<?php echo Yii::app()->baseURL; ?>/images/userfiles/<?php echo $model->photo; ?>" width="95%" />
			<?php endif; ?>
		</div>
	
		<div class="row" style="width:30%;float:left;">
			<?php echo $form->labelEx($model,'comment'); ?>
			<?php echo $form->textArea($model,'comment',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'comment'); ?>
		</div>
		
		<div style="clear:both;"></div>
	</div><br /><br />
	
	<?php echo $form->hiddenField($model,'token',array('size'=>60,'maxlength'=>250)); ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->