<?php

class PaypalController extends Controller
{
	public $layout = "//layouts/public";
	
	public function actionBuy($id){
               
		$modelOrder = Order::model()->findByPK($id);
		
		// Email to shop
		$body = "Se ha recibido un pedido en la tienda Un regalo perfecto. Acceda la panel de admistraci&oacute;nn para gestionar el mismo y comprobar si se ha realizado
		el pago a trav&eacute;s de Paypal.<br /><br />
		<b>Producto:</b> " . $modelOrder->product->name . "<br />
		<b>Tel&eacute;fono de contacto remitente:</b> " . $modelOrder->rphone . "<br /><br />			
		<a href='". Yii::app()->params->siteURL. Yii::app()->createURL("Admin") ."'>Ir al Panel de control</a>";	
		
		Functions::SendEmail(Yii::app()->params->appMailUser,"ventas@unregaloperfecto.es","Se ha recibido un pedido",$body);
		
		// Email to sender
		$body = "Hemos recibido su pedido en la tienda Un regalo perfecto. Ahora tiene que realizar el pago a trav�s de Paypal para que podamos enciarle
		el regalo al destinatario.<br /><br />
		<b>Producto:</b> " . $modelOrder->product->name . "<br />
		<b>Telefono de contacto remitente:</b> " . $modelOrder->rphone . "<br /><br />	
		Recordarle que el regalo es totalmente sorpresa y que nosotros no contactaremos con el destinario nada m�s que para entregarle el paquete.<br /><br />
		Reciba un cordial saludo del equipo de Un regalo perfecto<br /><br />
		";	
		
		Functions::SendEmail(Yii::app()->params->appMailUser,$modelOrder->remail,"Hemos recibido su pedido",$body);
		
		$modelProduct = $modelOrder->product;
				
		// set 
		$paymentInfo['Order']['theTotal'] = $modelProduct->price * $modelOrder->units;
		$paymentInfo['Order']['description'] = $modelProduct->name;
		$paymentInfo['Order']['quantity'] = $modelOrder->units;

		// call paypal 
		$result = Yii::app()->Paypal->SetExpressCheckout($paymentInfo); 
		//Detect Errors 
		if(!Yii::app()->Paypal->isCallSucceeded($result)){ 
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			Yii::app()->end();
			
		}else { 
			// send user to paypal 
			$token = urldecode($result["TOKEN"]);
			
			// add token to order 
			$modelOrder->token = $token;
			
			// if token is saved send customer to paypal checkout
			if($modelOrder->save()):
				$payPalURL = Yii::app()->Paypal->paypalUrl.$token; 
				$this->redirect($payPalURL); 
			endif;
		}
	}

	public function actionConfirm()
	{
		$token = trim($_GET['token']);
		$payerId = trim($_GET['PayerID']);
		
		$model = Order::model()->findByAttributes(array("token" => $token));
		
		$result = Yii::app()->Paypal->GetExpressCheckoutDetails($token);

		$result['PAYERID'] = $payerId; 
		$result['TOKEN'] = $token; 
		$result['ORDERTOTAL'] = $model->price * $model->units;

		//Detect errors 
		if(!Yii::app()->Paypal->isCallSucceeded($result)){ 
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'No hemos podido procesar su pago.';
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			Yii::app()->end();
		}else{ 
			
			$paymentResult = Yii::app()->Paypal->DoExpressCheckoutPayment($result);
			//Detect errors  
			if(!Yii::app()->Paypal->isCallSucceeded($paymentResult)){
				if(Yii::app()->Paypal->apiLive === true){
					//Live mode basic error message
					$error = 'No hemos podido procesar su pago.';
				}else{
					//Sandbox output the actual error message to dive in.
					$error = $paymentResult['L_LONGMESSAGE0'];
				}
				echo $error;
				Yii::app()->end();
			}else{
				//payment was completed successfully
				
				$model->status = 1;
		
				$model->save();
				
				$this->render('confirm');
			}
			
		}
	}
	
	
	
        
    public function actionCancel()
	{
		//The token of the cancelled payment typically used to cancel the payment within your application
		$token = $_GET['token'];
		
		$this->render('cancel');
	}
	
	public function actionDirectPayment(){ 
		$paymentInfo = array('Member'=> 
			array( 
				'first_name'=>'name_here', 
				'last_name'=>'lastName_here', 
				'billing_address'=>'address_here', 
				'billing_address2'=>'address2_here', 
				'billing_country'=>'country_here', 
				'billing_city'=>'city_here', 
				'billing_state'=>'state_here', 
				'billing_zip'=>'zip_here' 
			), 
			'CreditCard'=> 
			array( 
				'card_number'=>'number_here', 
				'expiration_month'=>'month_here', 
				'expiration_year'=>'year_here', 
				'cv_code'=>'code_here' 
			), 
			'Order'=> 
			array('theTotal'=>1.00) 
		); 

	   /* 
		* On Success, $result contains [AMT] [CURRENCYCODE] [AVSCODE] [CVV2MATCH]  
		* [TRANSACTIONID] [TIMESTAMP] [CORRELATIONID] [ACK] [VERSION] [BUILD] 
		*  
		* On Fail, $ result contains [AMT] [CURRENCYCODE] [TIMESTAMP] [CORRELATIONID]  
		* [ACK] [VERSION] [BUILD] [L_ERRORCODE0] [L_SHORTMESSAGE0] [L_LONGMESSAGE0]  
		* [L_SEVERITYCODE0]  
		*/ 
	  
		$result = Yii::app()->Paypal->DoDirectPayment($paymentInfo); 
		
		//Detect Errors 
		if(!Yii::app()->Paypal->isCallSucceeded($result)){ 
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			
		}else { 
			//Payment was completed successfully, do the rest of your stuff
		}

		Yii::app()->end();
	} 
}