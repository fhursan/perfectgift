<div class="home">

	<?php 
		$this->breadcrumbs=array(
			$model->category->name => array('site/category', 'id'=>$model->categoryID),
			$model->name,
		);
	?>
		
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'homeLink' => CHtml::link('Inicio', Yii::app()->homeUrl),
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<div id="contentSlider">
		<div class="flexslider">
			
			<ul class="slides">
				
				<?php 
					$criteria=new CDbCriteria;
					$criteria->order = "ID asc";
					$criteria->condition = "productID = :productid";
					$criteria->params = array(':productid' => $model->ID);
					
					$modelPhotos = Photo::model()->findAll($criteria); 
				?>
				
				<?php foreach($modelPhotos as $photo): ?>
				
					<li data-thumb="<?php echo Yii::app()->baseURL; ?>/images/userfiles/<?php echo $photo->path; ?>">
						<img src="<?php echo Yii::app()->baseURL; ?>/images/userfiles/<?php echo $photo->path; ?>" style="max-height: 600px;" />
					</li>
				
				<?php endforeach; ?>
				
			</ul><!-- slides -->
						
		</div><!-- flexslider -->
		
		<div style="clear: both;"></div>
		
	</div><!-- contentSlider -->
	
	<div style="clear: both;"></div>
	
	<div id="productData">
		<h1><? echo $model->name; ?></h1>
		<p><? echo $model->description; ?></p>
		
		<div class="price">
			<p>
				Precio por unidad: <br />
				<span><?php echo str_replace('.',',',number_format($model->price, 2)); ?><span class="euro">&euro;</span></span><br />
				<span class="little">IVA y transporte incluido</span>
			</p>
			<a href="<?php echo Yii::app()->createURL("site/buy", array("id"=>$model->ID)); ?>" class="button">COMPRAR</a>
		</div><!-- price -->
		
		<div class="buttons">
			<p>Compartelo con tus amigos:</p>
			<?php 
				$this->widget('ext.social-sharing.SocialShareWidget', array(
					'url' => Yii::app()->params->siteURL . Yii::app()->createURL("site/product", array("id"=>$model->ID)),
					'services' => array('facebook', 'twitter', 'google'),
					'htmlOptions' => array('class' => 'socialShare'),
					'popup' => yes,
				));
			?>
			
			<div style="clear: both;"></div>
		</div><!-- buttons -->
		
		<div style="clear: both;"></div>
		
	</div><!-- productData -->
	
	<div id="otherProducts">
		
		<h1>M&aacute;s productos de esta categor&iacute;a:</h1>
		
		<?php if($modelProducts): ?>
			
			<?php foreach($modelProducts as $product) : ?>
			
				<?php $modelPhoto = Photo::model()->findByAttributes(array('productID' => $product->ID), array('order' => 'ID desc')); ?>
						
				<div class="product left">
										
					<div id="caption">
					
						<a href="<?php echo Yii::app()->createURL('site/product', array('id'=>$product->ID)); ?>" class="clear">
							<span id="minislideName"><?php echo Functions::stringCut($product->name, 36) . " / " . str_replace('.',',',number_format($product->price, 2)); ?>&euro;</span>
						</a>
					
					</div>
					
					<a href="<?php echo Yii::app()->createURL('site/product', array('id'=>$product->ID)); ?>">
						<img src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => $modelPhoto->path)); ?>" />
					</a>
					
				</div><!-- product -->
								
			<?php endforeach; ?>
		
		<?php else : ?>
			<p>Esta categoria no tiene productos</p>
		<?php endif; ?>
		
		<div style="clear:both;"></div>
	</div><!-- products -->
	
</div><!-- home -->

<script>
	$(window).load(function(){		
		$('.flexslider').flexslider({
	    	animation: "slide",
	    	slideshow: false,
	    	controlNav: "thumbnails",
	    	directionNav: false,
	    	start: function(){
	    		$(".flex-viewport").animate({'max-height' : $(".flex-active-slide").css('height')}, 200);
	    		$("#contentSlider").animate({'max-height' : $(".flex-active-slide").css('height'), 'padding-bottom' : '4%'}, 200);
		    	$("#slideName").html($('li.flex-active-slide img').attr('alt'));
		    	$("#slideName").parent().attr('id', $('li.flex-active-slide img').attr('id'));
	    	},
	    	
	    	after: function(){
	    		$(".flex-viewport").animate({'max-height' : $(".flex-active-slide").css('height')}, 200);
	    		$("#contentSlider").animate({'max-height' : $(".flex-active-slide").css('height'), 'padding-bottom' : '4%'}, 200);
		    	$("#slideName").html($('li.flex-active-slide img').attr('alt'));
		    	$("#slideName").parent().attr('id', $('li.flex-active-slide img').attr('id'));
	    	},
	    });
	    
	    $(".loader").hide();
		$(".home").fadeIn();
	    
	    if($(".flex-control-thumbs").length){
	    	return false
	    }else{
		    $("#contentSlider .flex-viewport").css({ 'max-width': '100%', 'margin-right': '0%' });
		    $(".flexslider .slides img").css({ 'max-width': '100%' });

	    }
	});
</script>
