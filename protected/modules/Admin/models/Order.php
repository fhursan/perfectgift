<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $ID
 * @property integer $customerID
 * @property string $name
 * @property string $surname
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $rname
 * @property string $rsurname
 * @property string $DNI
 * @property string $remail
 * @property string $rphone
 * @property integer $units
 * @property string $price
 * @property string $date
 * @property integer $status
 */
class Order extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('customerID, units, price, date', 'required'),
			array('customerID, units, status', 'numerical', 'integerOnly'=>true),
			array('name, rname', 'length', 'max'=>100),
			array('surname, email, rsurname, remail, photo', 'length', 'max'=>150),
			array('address, raddress', 'length', 'max'=>250),
			array('phone, rphone', 'length', 'max'=>20),
			array('DNI', 'length', 'max'=>12),
			array('price', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, customerID, name, surname, address, email, phone, rname, rsurname, DNI, remail, rphone, raddress, photo, comment, units, price, date, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'productID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'customerID' => 'Cliente',
			'name' => 'Nombre',
			'surname' => 'Apellidos',
			'address' => 'Direcci&oacute;n',
			'email' => 'Email',
			'phone' => 'Telefono',
			'rname' => 'Nombre rem',
			'rsurname' => 'Apellido rem',
			'DNI' => 'DNI',
			'remail' => 'Email rem',
			'rphone' => 'Telefono rem.',
			'units' => 'Unidades',
			'price' => 'Precio',
			'date' => 'Fecha entrega',
			'status' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('customerID',$this->customerID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('rname',$this->rname,true);
		$criteria->compare('rsurname',$this->rsurname,true);
		$criteria->compare('DNI',$this->DNI,true);
		$criteria->compare('remail',$this->remail,true);
		$criteria->compare('rphone',$this->rphone,true);
		$criteria->compare('units',$this->units);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function status($id)
	{
		switch($id) :
		
			case 0:
				return "En proceso";
				
			case 1:
				return "Pagado";
			
			case 2: 
				return "Enviado y facturado";
				
			case 3:
				return "Entregado";
				
		endswitch;
	}
}