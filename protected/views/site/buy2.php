<div class="home">

	<?php 
		$this->breadcrumbs=array(
			"Resumen pedido",
		);
	?>
		
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'homeLink' => CHtml::link('Inicio', Yii::app()->homeUrl),
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<div id="productData">
	
		<h1>Resumen del pedido:</h1>
	
		<div class="productPhoto">
			<img src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => Product::model()->getPhoto($modelProduct->ID))); ?>" style="width: 100%;" />
		</div><!-- productPhoto -->
		
			<div class="productText">
				<p><? echo Functions::stringCut($modelProduct->description, 80); ?></p>
				<div style="float:right;">
					<p>
						Total: <br />
						<span style="font-size: 1.4em;"><span id="priceFinal"><?php echo str_replace('.',',',number_format($modelProduct->price * $model->units, 2)); ?></span><span class="euro">&euro;</span></span><br />
						<span class="little">IVA y transporte incluido</span>
					</p>
				</div>
				
				<div style="float:left; width: 40%;">
					<p>
						Unidades a enviar: <br /> 
							<input type="hidden" id="price" name="price" value="<?php echo $modelProduct->price; ?>" />
							<input type="hidden" name="product" id="product" value="<?php echo $modelProduct->ID; ?>" /> 
							<input type="text" id="unit" name="unit" style="width: 25%; text-align: center;" value="<?php echo $model->units; ?>" readonly /> x <?php echo str_replace('.',',',number_format($modelProduct->price, 2)); ?>
					</p>
				</div>
			</div><!-- productText -->
	
			<div style="clear: both;"></div><br /><br />
		
			<h1>Datos de destinatario</h1>
						
			<div style="float:left; width: 45%;" class="orderSummary">
				<p><label for="name">Fecha de entrega</label>: <span><?php echo date("d-m-Y", strtotime($model->date)); ?></span></p>
		        
				<p><label for="name">Nombre</label>: <span><?php echo $model->name; ?></span></p>
				
				<p><label for="surname">Apellidos</label>: <span><?php echo $model->surname; ?></span></p>
				
				<p><label for="surname">Email</label>: <span><?php echo $model->email; ?></span></p>
				
				<p><label for="surname">Tel&eacute;fono</label>: <span><?php echo $model->phone; ?></span></p>
			</div>
			
			<div style="float:right; width: 45%;" class="orderSummary">
				<?php if($model->photo and $model->photo != ""): ?>
					<p>
						<label for="surname">Foto</label>:<br /> 
						<img src="<?php echo Yii::app()->baseURL; ?>/images/userfiles/<?php echo $model->photo; ?>" width="50%" />
					</p>
				<?php endif; ?>
				<p><label for="surname">Dedicatoria</label>:<br /> <span><?php echo $model->comment; ?></span></p>
			</div>
			
			<div style="clear: both;"></div><br />
			
			<h1>Datos del remitente</h1>
			
			<div style="float:left; width: 45%;" class="orderSummary">
				<p><label for="name">Nombre remit.</label>: <span><?php echo $model->rname; ?></span></p>
				<p><label for="name">Apellidos remit.</label>: <span><?php echo $model->rsurname; ?></span></p>
				<p><label for="name">DNI remit.</label>: <span><?php echo $model->DNI; ?></span></p>
			</div>
			
			<div style="float:right; width: 45%;" class="orderSummary">
				<p><label for="name">Email remit.</label>: <br /><span><?php echo $model->remail; ?></span></p>
				<p><label for="name">Phone remit.</label>: <span><?php echo $model->rphone; ?></span></p>
			</div>
						
			<div style="clear: both;"></div>
			
			<div style="margin-top: 5%;">
				<a href="<?php echo Yii::app()->createURL("paypal/buy", array("id"=>$model->ID)); ?>" class="button">PAGAR</a>
			</div>
			
			
			<div style="clear: both;"></div>
		
	</div><!-- productData -->
	
</div><!-- home -->

<script>
	$(document).ready(function(){		
		$("#addUnit").click(function(){
			if($("#unit").val() != 25)
			{
				$("#unit").val(parseInt($("#unit").val()) + 1);
				var price;
				price = (((parseFloat($("#price").val()) * parseFloat($("#unit").val())) *100) / 100);
				$("#priceFinal").html( price.toString().replace(".",",") );	
			}
		});
		
		$("#quitUnit").click(function(){
			if($("#unit").val() != 1)
			{
				$("#unit").val(parseInt($("#unit").val()) - 1);
				var price;
				price = (((parseFloat($("#price").val()) * parseFloat($("#unit").val())) *100) / 100);
				$("#priceFinal").html( price.toString().replace(".",",") );	
			}
		});
		
		$("#submitForm").click(function(){
			var error = "";
			
			var year = parseInt($("#date").val().substring(6, 10));
			var month = parseInt($("#date").val().substring(3, 5) - 1);
			var day = parseInt($("#date").val().substring(0, 2));
			
			var date = new Date(year, month, day); 
			var today = new Date();
			
			console.log("date: " + date.getTime() + ", " + date);
			console.log("today: " + today.getTime() + ", " + today);
			
			if(date.getTime() <= today.getTime()){ error = error + "- La fecha de entrega debe ser posterior\n\r"; $("#date").addClass("error");  } else { $("#date").removeClass("error"); }
						
			if($("#date").val() == ""){ error = error + "- Debe de poner una fecha de entrega\n\r"; $("#date").addClass("error"); } else { $("#date").removeClass("error"); }
			
			if($("#name").val() == ""){ error = error + "- Debe de poner el nombre del destinatario\n\r"; $("#name").addClass("error"); } else { $("#name").removeClass("error"); }
			if($("#surname").val() == ""){ error = error + "- Debe de poner los apellidos del destinatario\n\r"; $("#surname").addClass("error"); } else { $("#surname").removeClass("error"); }
			if($("#address").val() == ""){ error = error + "- Debe de poner la direccion de entrega del destinatario\n\r"; $("#address").addClass("error"); } else { $("#address").removeClass("error"); }
			if($("#email").val() == ""){ error = error + "- Debe de poner el email del destinatario\n\r"; $("#email").addClass("error"); } else { $("#email").removeClass("error"); }
			if($("#phone").val() == ""){ error = error + "- Debe de poner el telefono del destinatario\n\r"; $("#phone").addClass("error"); } else { $("#phone").removeClass("error"); }
			if($("#rname").val() == ""){ error = error + "- Debe de poner el nombre del remitente\n\r"; $("#rname").addClass("error"); } else { $("#rname").removeClass("error"); }
			if($("#rsurname").val() == ""){ error = error + "- Debe de poner los apellidos del remitente\n\r"; $("#rsurname").addClass("error"); } else { $("#rsurname").removeClass("error"); }
			if($("#DNI").val() == ""){ error = error + "- Debe de poner el DNI del remitente\n\r"; $("#DNI").addClass("error"); } else { $("#DNI").removeClass("error"); }
			if($("#raddress").val() == ""){ error = error + "- Debe de poner la direccion del remitente\n\r"; $("#raddress").addClass("error"); } else { $("#raddress").removeClass("error"); }
			if($("#remail").val() == ""){ error = error + "- Debe de poner el email del remitente\n\r"; $("#remail").addClass("error"); } else { $("#remail").removeClass("error"); }
			if($("#rphone").val() == ""){ error = error + "- Debe de poner el telefono del remitente\n\r"; $("#rphone").addClass("error"); } else { $("#rphone").removeClass("error"); }
			if(!$('#lopd').prop('checked'))	{ error = error + "- Debe aceptar las condiciones de uso\n\r"; }
			
			if(error != "")
			{
				alert(error)
;			}
			else
			{
				$("#buy-form").submit();
			}
		});
		
		$(".loader").hide();
		$(".home").fadeIn();
	});
</script>
