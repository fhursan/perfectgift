<div class="home">
	
	<h3 style="margin-top:10%;">&iexcl;Enhorabuena! Su pago se ha procesado correctamente</h3>
	<p>
		Su pago se ha procesado correctamente y su pedido se tramitar&aacute; en breve. Recibir&aacute; en su email la factura del mismo en los pr&oacute;ximos minutos.<br /><br />
		Gracias por su tiempo, y si tiene alguna duda puede ponerse en contacto con nosotros a trav&eacute;s de <a href="mailto: info@unregaloperfecto.es">info@unregaloperfecto.es</a>
	</p><br />
	
	<p><a href="<?php echo Yii::app()->createURL("site/index"); ?>" class="euro">Volver a la tienda de Un regalo perfecto.</a></p>
	
</div>

<script>
	$(window).load(function(){
		$(".loader").hide();
		$(".home").fadeIn();
	});
</script>