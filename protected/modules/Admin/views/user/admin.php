<?php
/* @var $this UserController */
/* @var $model User */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Usuarios'=>array('index'),
		'Listado',
	),
));

?>

<h1>Gesti&oacute;n de usuarios</h1>

<a href="<?php echo Yii::app()->createURL("/Admin/user/create"); ?>" class="blueButton">CREAR USUARIO</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'template'=>'{items} {summary} {pager}',
	'columns'=>array(
		array(
            'name'=>'name',
            'value'=>'$data->name',
            'headerHtmlOptions'=>array(
                'style'=>'width:30%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'email',
            'value'=>'$data->email',
            'headerHtmlOptions'=>array(
                'style'=>'width:30%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'username',
            'value'=>'$data->username',
            'headerHtmlOptions'=>array(
                'style'=>'width:30%;text-align:left !important;',
            ),
        ),
		/*
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete}',
			'buttons'=>array(
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
