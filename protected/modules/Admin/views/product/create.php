<?php
/* @var $this ProductController */
/* @var $model Product */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Productos'=>array('index'),
		'Crear',
	),
));

?>

<h1>Crear producto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>