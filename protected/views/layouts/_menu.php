<div class="leftbar">
	
	<a href="<?php echo Yii::app()->createURL('site/index'); ?>">
		<img class="logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" />
	</a>
	
	<h1>Cat&aacute;logo</h1>
	
	<ul>
		<?php foreach(Category::model()->findAll(array('order' => 'name asc')) as $category) : ?>
			<li>
				<a href="<?php echo Yii::app()->createURL('site/category', array('id'=>$category->ID)); ?>"><?php echo $category->name; ?></a>
			</li>
		<?php endforeach; ?>
	</ul>

</div><!-- leftbar -->