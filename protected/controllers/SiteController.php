<?php

class SiteController extends Controller
{
	public $layout = "//layouts/public";
	
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = "//layouts/login";
		
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->createURL("/Admin"));
	}
	
	
	/**
	 * Action to show one single product on frontend
	 */
	public function actionProduct($id)
	{
		$model = Product::model()->findByPK($id);
		
		$modelProducts = Product::model()->findAllByAttributes(array("categoryID" => $model->categoryID), array("limit" => 6, "condition" => "ID <>".$model->ID));
		
		if(count($model) > 0) :
			$this->render('product', array('model' => $model, 'modelProducts' => $modelProducts));
		else :
			throw new CHttpException(404,'El producto que buscas no existe.');
		endif;
	}
	
	/**
	 * Action to show all category's products
	 */
	public function actionCategory($id)
	{
		$model = Category::model()->findByPK($id);
		
		if(count($model) > 0) :
			$this->render('category', array('model' => $model));
		else :
			throw new CHttpException(404,'La categoria que buscas no existe.');
		endif;
	}
	
	/**
	 * Action to show all category's products
	 */
	public function actionBuy($id)
	{
		$model = Product::model()->findByPK($id);
		
		if(count($model) > 0) :
			if($_POST["date"]) :
				// Create customer
				if(!$modelCustomer = Customer::model()->findByAttributes(array("email" => $_POST["remail"])))
					$modelCustomer = new Customer;
					
				$modelCustomer->name = $_POST["rname"];
				$modelCustomer->surname = $_POST["rsurname"];
				$modelCustomer->email = $_POST["remail"];
				$modelCustomer->phone = $_POST["rphone"];
				$modelCustomer->address = $_POST["raddress"];
				$modelCustomer->date = date("Y-m-d");
				
				if($modelCustomer->save()):
					$modelOrder = new Order;
					$modelOrder->customerID = $modelCustomer->ID;
					$modelOrder->productID = $_POST["product"];
					$modelOrder->name = $_POST["name"];
					$modelOrder->surname = $_POST["surname"];
					$modelOrder->address = $_POST["address"];
					$modelOrder->email = $_POST["email"];
					$modelOrder->phone = $_POST["phone"];
					$modelOrder->rname = $_POST["rname"];
					$modelOrder->rsurname = $_POST["rsurname"];
					$modelOrder->DNI = $_POST["DNI"];
					$modelOrder->remail = $_POST["remail"];
					$modelOrder->rphone = $_POST["rphone"];
					$modelOrder->comment = $_POST["comment"];
					$modelOrder->photo = $_POST["photo"];
					$modelOrder->units = $_POST["unit"];
					$modelOrder->date = date("Y-m-d", strtotime($_POST["date"]));
					$modelOrder->price = $_POST["price"];
					$modelOrder->status = 0;
					
					Yii::import('application.extensions.image.Image');
					
					if($img = CUploadedFile::getInstanceByName('photo')) :
				
						$path = rand(1000, 9999) . "_" . $img->name;
						
						$img->saveAs(Yii::app()->params->imgPath . $path);
									
						$img = New Image(Yii::app()->params->imgPath . $path);
						
						if($img->width > $img->height) :
					    	$img->resize(800, 600, Image::HEIGHT);
					    else :
					    	$img->resize(800, 600, Image::WIDTH);
					    endif;
					    
					    $img->save(Yii::app()->params->imgPath . $path);
					
					else :
						
						$path = "";
						
					endif;
					
					$modelOrder->photo = $path;
					
					if($modelOrder->save()) :
						
						$this->redirect(array('buy2', 'id' => $modelOrder->ID));
					else :
						var_dump($modelOrder->getErrors());
					endif;
				else: 
					var_dump($modelCustomer->getErrors());
					$this->render('buy', array('model' => $model));
				endif;
			else: 
				$this->render('buy', array('model' => $model));
			endif;
		else :
			throw new CHttpException(404,'El producto que buscas no existe.');
		endif;
	}
	
	
	public function actionBuy2($id)
	{
		$model = Order::model()->findByPK($id);
		$modelProduct = $model->product;
		
		$this->render('buy2', array('model'=>$model, 'modelProduct'=>$modelProduct));
	}
	/**
	 * Render image cropped
	 */
	public function actionRenderImage($path)
	{
		Yii::import('application.extensions.image.Image');
	    
	    $path = $path != "" ? $path : "nophoto.png";
	    
	    $img = New Image(Yii::app()->params->imgPath.$path);
	    
	    if($img->width > $img->height) :
	    	$img->resize(800, 600, Image::HEIGHT)->crop(800,600, 'center')->quality(60);
	    else :
	    	$img->resize(800, 600, Image::WIDTH)->crop(800,600, 'center')->quality(60);
	    endif;
	    
	    $img->render();
	}
	
	/**
	 * Render image cropped
	 */
	public function actionRenderImageMini($path)
	{
		Yii::import('application.extensions.image.Image');
	    
	    $img = New Image(Yii::app()->params->imgPath.$path);
	    
	    if($img->width > $img->height) :
	    	$img->resize(80, 60, Image::HEIGHT);
	    else :
	    	$img->resize(80, 60, Image::WIDTH);
	    endif;
	    
	    $img->crop(80,60, 'center')->quality(60);
	    
	    $img->render();
	}
}