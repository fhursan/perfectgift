<?php
/* @var $this SerieController */
/* @var $model Serie */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'serie-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'serie'); ?>
		<?php echo $form->textField($model,'serie',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'serie'); ?>
	</div>
	
	<div class="row" style="width: 20%; float: left;">
		<?php echo $form->labelEx($model,'default'); ?>
		<div class="compactRadioGroup">
		<?php echo $form->radioButtonList($model, 'default',
                    array(  0 => 'No',
                            1 => 'Si')); ?>
		</div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->