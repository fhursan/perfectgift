<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'El Regalo Perfecto',
	'language'=>'es',
	'sourceLanguage'=>'es',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.Admin.models.*',
	),

	'modules'=>array(
		'Admin',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'chuloc0',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		
		'image'=>array(
        	'class'=>'application.extensions.image.CImageComponent',
             // GD or ImageMagick
             'driver'=>'GD',
             // ImageMagick setup path
             'params'=>array('directory'=>dirname(__FILE__).'/images/userfiles'),
        ),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		'Paypal' => array(
		    'class'=>'application.components.Paypal',
		    'apiUsername' => 'Fhursan_api1.gmail.com',
		    'apiPassword' => '364773MA24WREH9E',
		    'apiSignature' => 'A.voKV5yft5kTKWJPpA7tnzsTVsNAP6sd16OPvB5cZoQz6Cg9HPtVRlR',
		    'apiLive' => true,
		 
		    'returnUrl' => 'paypal/confirm/', //regardless of url management component
		    'cancelUrl' => 'paypal/cancel/', //regardless of url management component
		 
		    // Default currency to use, if not set USD is the default
		    'currency' => 'EUR',
		 
		    // Default description to use, defaults to an empty string
		    //'defaultDescription' => '',
		 
		    // Default Quantity to use, defaults to 1
		    //'defaultQuantity' => '1',
		 
		    //The version of the paypal api to use, defaults to '3.0' (review PayPal documentation to include a valid API version)
		    //'version' => '3.0',
		),
		
		/*
		
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=perfectgift',
			'emulatePrepare' => true,
			'username' => 'regalo_user',
			'password' => 'regalo00Z',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		'ePdf' => array(
	        'class' => 'ext.yii-pdf.EYiiPdf',
	        'params' => array(
	            'mpdf' => array(
	                'librarySourcePath' => 'application.vendors.mpdf.*',
	                'constants'         => array(
	                    '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
	                ),
	                'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
	                'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
	                    'mode'              => '', //  This parameter specifies the mode of the new document.
	                    'format'            => 'A4', // format A4, A5, ...
	                    'default_font_size' => 0, // Sets the default document font size in points (pt)
	                    'default_font'      => '', // Sets the default font-family for the new document.
	                    'mgl'               => 15, // margin_left. Sets the page margins for the new document.
	                    'mgr'               => 15, // margin_right
	                    'mgt'               => 16, // margin_top
	                    'mgb'               => 16, // margin_bottom
	                    'mgh'               => 9, // margin_header
	                    'mgf'               => 9, // margin_footer
	                    'orientation'       => 'P', // landscape or portrait orientation
	                )
	            ),
	            'HTML2PDF' => array(
	                'librarySourcePath' => 'application.vendors.html2pdf.*',
	                'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
	                'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
	                    'orientation' => 'P', // landscape or portrait orientation
	                    'format'      => 'A4', // format A4, A5, ...
	                    'language'    => 'en', // language: fr, en, it ...
	                    'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
	                    'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
	                    'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
	                )
	            )
	        ),
	    ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'fhurtado@daimonconsulting.com',
		'appMailServer'=>'mail.unregaloperfecto.es',
        'appMailPort'=>25,
        'appMailUser'=>'no-reply@unregaloperfecto.es',
        'appMailPass'=>'Regalo00Z',
		'imagesURL'=>'../..//images/userfiles/',
		'imgPublicPath'=>'/images/userfiles/',
		'siteURL'=>'http://www.unregaloperfecto.es',
		'imgPath'=>'./images/userfiles/',
		'name'=>'Un regalo perfecto SL',
		'DNI'=>'11111111A',
		'address'=>'C/pollensa 4 Edificio Atenea',
	),
);