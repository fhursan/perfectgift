<?php

class InvoiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		$Admin = "isset(Yii::app()->user->role) && (Yii::app()->user->role==='admin')";
		$User  = "isset(Yii::app()->user->role) && (Yii::app()->user->role==='user')";
            
        return array(
        	array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('upload','printpublic'),
				'users'=>array('*'),
			),
        	array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(),
				'users'=>array('@'),
                'expression'=>$User,
			),
            
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','admin','index','print','send'),
				'users'=>array('@'),
                'expression'=>$Admin,
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Invoice;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invoice']))
		{
			$model->attributes=$_POST['Invoice'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invoice('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invoice']))
			$model->attributes=$_GET['Invoice'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Invoice the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Invoice::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Invoice $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invoice-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	/*
	 * Print invoice
	 */
	public function actionPrint($id)
	{
		$model = $this->loadModel($id);
		
		$criteria = new CDbCriteria();
		$criteria->condition = "InvoiceID = :invoiceid";
		$criteria->params = array(':invoiceid' => $model->ID);
		
		// Print invoice
		set_time_limit(600);
    		
        # mPDF
        $pdf = Yii::app()->ePdf->mpdf('', 'A4', '','','','','','','','','P');

        $pdf->writeHTMLfooter=false;
        $pdf->writeHTMLheader=false;
        $pdf->DeflMargin=25;
        $pdf->DefrMargin=25;
        $pdf->tMargin=15;
        $pdf->bMargin=15;

        $pdf->w=297;   //manually set width
        $pdf->h=209.8; //manually set height
        
        $pdf->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.images') . '/logo.png'));
        
        $pdf->WriteHTML($this->renderPartial('_print', array('model'=>$model), true));
        
        # Outputs ready PDF
	    $pdf->Output('Factura_' . $model->serie->serie . '-' . $model->number . '.pdf','D');
	}
	
	/*
	 * Print invoice
	 */
	public function actionPrintPublic($id)
	{
		$model = $this->loadModel(base64_decode($id));
		
		$criteria = new CDbCriteria();
		$criteria->condition = "InvoiceID = :invoiceid";
		$criteria->params = array(':invoiceid' => $model->ID);
		
		// Print invoice
		set_time_limit(600);
    		
        # mPDF
        $pdf = Yii::app()->ePdf->mpdf('', 'A4', '','','','','','','','','P');

        $pdf->writeHTMLfooter=false;
        $pdf->writeHTMLheader=false;
        $pdf->DeflMargin=25;
        $pdf->DefrMargin=25;
        $pdf->tMargin=15;
        $pdf->bMargin=15;

        $pdf->w=297;   //manually set width
        $pdf->h=209.8; //manually set height
        
        $pdf->WriteHTML(CHtml::image(Yii::getPathOfAlias('webroot.images') . '/logo.png'));
        
        $pdf->WriteHTML($this->renderPartial('_print', array('model'=>$model), true));
        
        # Outputs ready PDF
	    $pdf->Output('Factura_' . $model->serie->serie . '-' . $model->number . '.pdf','D');
	}
	
	/*
	 * Send invoice to customer email
	 */
	public function actionSend($id)
	{
		$model = $this->loadModel($id);
		
		$customerEmail = $model->order->remail;
		
		// Email to shop
		$body = "Su regalo ha sido facturado y enviado al destinatario. Puede descargar su factura en el siguiente enlace: <br /><br />			
		<a href='". Yii::app()->params->siteURL. Yii::app()->createURL("Admin/invoice/printpublic", array("id" => base64_encode($id))) ."'>Descargar Factura</a><br /><br />
		Reciba un cordial saludo del equipo de Un regalo perfecto<br /><br />";	
		
		Functions::SendEmail(Yii::app()->params->appMailUser,$customerEmail,"Pedido enviado y factura",$body);
		
		$this->redirect(array("invoice/index"));
	}
}
