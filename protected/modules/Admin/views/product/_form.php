<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div style="width: 45%; float:left;">
		<?php echo $form->errorSummary($model); ?>
	
		<div class="row">
			<?php echo $form->labelEx($model,'categoryID'); ?>
			<?php echo $form->dropDownList($model,'categoryID', CHtml::listData(Category::model()->findAll(array("order"=>"name")), 'ID', 'name'), 
												array('empty'=>'-- Selecciona la categoria --', 'style' => 'width: 50%;')); ?>
		</div>
	
		<div class="row">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('style'=>'width: 100%;','maxlength'=>100)); ?>
		</div>
	
		<div class="row">
			<?php echo $form->labelEx($model,'description'); ?>
			<?php echo $form->textArea($model,'description',array('rows'=>6, 'style'=>'width: 100%;')); ?>
		</div>
	
		<div class="row" style="width:15%;float:left;">
			<?php echo $form->labelEx($model,'price'); ?>
			<?php echo $form->textField($model,'price',array('style'=>'width: 80%;','maxlength'=>8,'value'=>str_replace(".",",",$model->price))); ?>
		</div>
	
		<div class="row" style="width:15%;float:left;margin-left:5%;">
			<?php echo $form->labelEx($model,'VAT'); ?>
			<?php echo $form->dropDownList($model,'VAT', CHtml::listData(VAT::model()->findAll(array("order"=>"ID")), 'name', 'name'), 
												array('empty'=>'IVA', 'style' => 'width: 100%;')); ?>
		</div>
		
		<div style="clear: both;"></div>
		
		<div class="row">
			<?php echo $form->checkBox($model,'promo'); ?>
			<?php echo $form->labelEx($model,'promo', array('style'=>'display: inline;')); ?>
		</div>
	</div>
	
	<div style="width: 45%; float:right;" id="galeria">
		<?php echo $this->renderPartial('_photo', array('model'=>$model, 'modelPhotos'=>$modelPhotos)); ?>
	</div>
	
	<div style="clear: both;"></div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->