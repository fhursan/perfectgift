<?php

/**
 * This is the model class for table "invoice".
 *
 * The followings are the available columns in table 'invoice':
 * @property string $ID
 * @property integer $orderID
 * @property string $charge
 * @property integer $number
 * @property integer $serieID
 * @property string $price
 * @property integer $customerID
 *
 * The followings are the available model relations:
 * @property Serie $iD
 * @property Order $order
 * @property Customer $customer
 */
class Invoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'invoice';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orderID, number, serieID, price, customerID', 'required'),
			array('orderID, number, serieID, customerID', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>10),
			array('charge', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, orderID, charge, number, serieID, price, customerID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iD' => array(self::BELONGS_TO, 'Serie', 'ID'),
			'order' => array(self::BELONGS_TO, 'Order', 'orderID'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'customerID'),
			'serie' => array(self::BELONGS_TO, 'Serie', 'serieID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'orderID' => 'Pedido',
			'charge' => 'Concepto',
			'number' => 'Num.',
			'serieID' => 'Serie',
			'price' => 'PVP',
			'customerID' => 'Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('orderID',$this->orderID);
		$criteria->compare('charge',$this->charge,true);
		$criteria->compare('number',$this->number);
		$criteria->compare('serieID',$this->serieID);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('customerID',$this->customerID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}