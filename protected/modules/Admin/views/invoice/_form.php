<?php
/* @var $this InvoiceController */
/* @var $model Invoice */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invoice-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>


	<?php echo $form->hiddenField($model,'orderID'); ?>
	
	<div class="row" style="float:left; width: 10%; margin-right: 5%;">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
              'model'=>$model,
              'attribute'=>'date',
              'value'=>$model->date,
              // additional javascript options for the date picker plugin
              'options'=>array(
                'changeYear'=>true,
                'yearRange'=>'2000:2050',
                'language'=>'es',
                'dateFormat'=>'yy-mm-dd',
                'monthNames' => array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"),
                'monthNamesShort' => array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"),
                'dayNames' => array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"),
                'dayNamesMin' => array('Do','Lu','Ma','Mi','Ju','Vi','Sa'),
                'defaultDate'=>$model->date,
                'firstDay'=>1,
              ),
              'htmlOptions'=>array(
                'placeholder'=>'yyyy-mm-dd',
                'style'=>'width:100%;',
              ),
            ));
        ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
	
	<div class="row" style="float:left; width: 15%; margin-right: 2%;">
		<?php echo $form->labelEx($model,'serieID'); ?>
		<?php echo $form->dropDownList($model,'serieID', CHtml::listData(Serie::model()->findAll(array("order"=>"serie")), 'ID', 'serie'), 
												array('empty'=>'-- Selecciona la serie --', 'style' => 'width: 100%;')); ?>
		<?php echo $form->error($model,'serieID'); ?>
	</div>

	<div class="row" style="float:left; width: 15%;">
		<?php echo $form->labelEx($model,'number'); ?>
		<?php echo $form->textField($model,'number'); ?>
		<?php echo $form->error($model,'number'); ?>
	</div>
		
	<div style="clear: both;"></div>

	<div class="row">
		<?php echo $form->labelEx($model,'charge'); ?>
		<?php echo $form->textField($model,'charge', array('style' => 'width: 50%;')); ?>
		<?php echo $form->error($model,'charge'); ?>
	</div>
	
	<div class="row" style="float:left; width: 25%; margin-right: 5%;">
		<?php echo $form->labelEx($model,'customerID'); ?>
		<?php echo $form->dropDownList($model,'customerID', CHtml::listData(Customer::model()->findAll(array("order"=>"name")), 'ID', 'name'), 
												array('empty'=>'-- Selecciona el cliente --', 'style' => 'width: 100%;')); ?>
		<?php echo $form->error($model,'customerID'); ?>
	</div>

	<div class="row" style="float:left; width: 10%;;">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>
	
	<div style="clear: both;"></div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->