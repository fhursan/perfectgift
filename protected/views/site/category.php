<div class="home">

	<?php 
		$this->breadcrumbs=array(
			$model->name,
		);
	?>
		
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'homeLink' => CHtml::link('Inicio', Yii::app()->homeUrl),
		)); ?><!-- breadcrumbs -->
	<?php endif?>
	
	<div id="products">
		<?php if($model->products): ?>

			<?php $index = 0; ?>
			
			<?php foreach($model->products as $product) : ?>
			
				<?php $modelPhoto = Photo::model()->findByAttributes(array('productID' => $product->ID), array('order' => 'ID desc')); ?>
				
				<?php $left = $index % 2 == 0 ? "left" : ""; // Odd or even ?>
		
				<div class="product <?php echo $left; ?>">
					
					<div class="spinner"><img src="<?php echo Yii::app()->baseURL; ?>/images/spinner.gif" /></div>
					
					<div id="caption">
					
						<a href="<?php echo Yii::app()->createURL('site/product', array('id'=>$product->ID)); ?>" class="clear">
							<span id="minislideName"><?php echo Functions::stringCut($product->name, 36) . " / " . str_replace('.',',',number_format($product->price, 2)); ?>&euro;</span>
						</a>
					
					</div>
					
					<a href="<?php echo Yii::app()->createURL('site/product', array('id'=>$product->ID)); ?>">
						<img src="<?php echo Yii::app()->createURL("site/renderimage", array("path" => $modelPhoto->path)); ?>" />
					</a>
					
				</div><!-- product -->
				
				<?php $index++; ?>
				
			<?php endforeach; ?>
		
		<?php else : ?>
			<p>Esta categoria no tiene productos</p>
		<?php endif; ?>
		
		<div style="clear:both;"></div>
	</div><!-- products -->
	
</div><!-- home -->

<script>
	$(document).ready(function(){
		$(".loader").hide();
		$(".home").fadeIn();
		
		$(".product").each(function(){
			$(this).css({"min-height" : ($(this).width() / (4/2.97)) + 'px'});
		});
		
		$(".product img").load(function(){
			$(this).parent().parent().children(".spinner").hide();
		});
	});
</script>
