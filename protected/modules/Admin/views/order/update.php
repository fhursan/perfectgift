<?php
/* @var $this OrderController */
/* @var $model Order */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Pedidos'=>array('admin'),
		'Modificar',
	),
));

?>

<h1>Editar pedido</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>