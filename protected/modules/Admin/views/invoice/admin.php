<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Facturas'=>array('index'),
		'Listado',
	),
));

?>

<h1>Gesti&oacute;n de facturas</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'invoice-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'template'=>'{items} {summary} {pager}',
	'columns'=>array(
		array(
            'name'=>'serieID',
            'value'=>'$data->serie->serie',
            'filter' => false,
            'headerHtmlOptions'=>array(
                'style'=>'width:12%;text-align:right !important;',
            ),
        ),
		array(
            'name'=>'number',
            'filter'=>false,
            'value'=>'$data->number',
            'headerHtmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
        ),
        array(
        	'header'=>'Cliente',
            'name'=>'customer',
            'filter' => false,
            'value'=>'$data->customer->name . $data->customer->surname',
            'headerHtmlOptions'=>array(
                'style'=>'width:25%;text-align:left !important;',
            ),
        ),
		array(
            'name'=>'charge',
            'value'=>'$data->charge',
            'headerHtmlOptions'=>array(
                'style'=>'width:45%;text-align:left !important;',
            ),
        ),
		array(
            'name'=>'price',
            'filter'=>false,
            'value'=>'$data->price . "€"',
            'headerHtmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:5%;text-align:center !important;',
            ),
        ),
		/*
		'customerID',
		*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete} {invoice} {send}',
			'buttons'=>array(
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
        		'invoice' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/ico_pdf.png',
            		'label'=>'Ver factura',
            		'url'=>'Yii::app()->createURL("Admin/invoice/print", array("id"=>$data->ID))',
        		),
        		'send' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/ico_pdf_send.png',
            		'label'=>'Enviar factura al comprador',
            		'url'=>'Yii::app()->createURL("Admin/invoice/send", array("id"=>$data->ID))',
            		'click'=>'function(){alert("Se ha enviado al comprador un email con la factura.");}',
        		),            
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
