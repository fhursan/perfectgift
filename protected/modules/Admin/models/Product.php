<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $ID
 * @property integer $categoryID
 * @property string $name
 * @property string $description
 * @property string $price
 * @property integer $VAT
 *
 * The followings are the available model relations:
 * @property Photo[] $photos
 * @property Category $category
 */
class Product extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('categoryID, price, name', 'required'),
			array('categoryID, promo', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('price', 'length', 'max'=>10),
			array('VAT', 'length', 'max'=>3),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, categoryID, name, description, price, VAT, promo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'photos' => array(self::HAS_MANY, 'Photo', 'productID'),
			'category' => array(self::BELONGS_TO, 'Category', 'categoryID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'categoryID' => 'Categoria',
			'name' => 'Nombre',
			'description' => 'Descripci&oacute;n',
			'price' => 'Precio',
			'VAT' => 'IVA',
			'promo' => 'Producto destacado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('categoryID',$this->categoryID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('VAT',$this->VAT);
		$criteria->compare('promo',$this->promo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Get the first photo of the product. If it doesn't exist we setup no photo image.
	 */ 
	public function getPhoto($id)
	{
		if($path = Photo::model()->findByAttributes(array("productID" => $id))->path) : 
			return $path;
		else : 
			return "nophoto.png"; 
		endif;
	}
}