<?php
/* @var $this ProductController */
/* @var $model Product */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Productos'=>array('admin'),
		'Listado',
	),
));

?>

<h1>Gesti&oacute;n de productos</h1>

<a href="<?php echo Yii::app()->createURL("/Admin/product/create"); ?>" class="blueButton">CREAR PRODUCTO</a> 
<a href="<?php echo Yii::app()->createURL("/Admin/category/create"); ?>" class="blueButton">CREAR CATEGORIA</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'template'=>'{items} {summary} {pager}',
	'columns'=>array(
		array(
            'name'=>'categoryID',
            'value'=>'$data->category->name',
            'filter'=>CHtml::listData(Category::model()->findAll(array("order"=>"name")), 'ID', 'name'),
            'headerHtmlOptions'=>array(
                'style'=>'width:15%;text-align:left !important;',
            ),
        ),
        array(
            'type' => 'html',
            'value'=>'CHtml::image(Yii::app()->createURL("site/renderimagemini", array("path" => Product::model()->getPhoto($data->ID)))) ',
        ),
        array(
            'name'=>'name',
            'value'=>'$data->name',
            'headerHtmlOptions'=>array(
                'style'=>'width:30%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'description',
            'value'=>'$data->description',
            'filter'=>false,
            'headerHtmlOptions'=>array(
                'style'=>'width:40%;text-align:left !important;',
            ),
        ),
        array(
            'name'=>'price',
            'value'=>'str_replace(".",",",$data->price)',
            'filter'=>false,
            'headerHtmlOptions'=>array(
                'style'=>'width:7%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:7%;text-align:center !important;',
            ),
        ),
        array(
            'name'=>'VAT',
            'value'=>'$data->VAT',
            'filter'=>false,
            'headerHtmlOptions'=>array(
                'style'=>'width:7%;text-align:center !important;',
            ),
            'htmlOptions'=>array(
                'style'=>'width:7%;text-align:center !important;',
            ),
        ),
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{update} {delete}',
			'buttons'=>array(
        		'update' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/view.png',
            		'label'=>Yii::t('admin', 'Ver/Editar'),
        		),
        		'delete' => array(
            		'imageUrl'=>Yii::app()->request->baseUrl.'/images/delete.png',
            		'label'=>Yii::t('admin', 'Borrar'),
        		),
		    ),
		    
		),
	),
	'emptyText' => 'No hay registros. <a href="'.$this->createURL('create').'">Picha</a> para crear uno.',
    'summaryText' => 'Mostrando del {start} al {end} de {count} registro(s).',
)); ?>
