<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Facturas'=>array('index'),
		'Crear',
	),
));
?>

<h1>Crer factura</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>