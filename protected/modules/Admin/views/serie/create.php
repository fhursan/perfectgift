<?php
/* @var $this SerieController */
/* @var $model Serie */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Series'=>array('index'),
		'Crear',
	),
));
?>

<h1>Crear Serie</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>