<?php
/* @var $this ProductController */
/* @var $model Product */

$this->widget('zii.widgets.CBreadcrumbs', array(
    'homeLink'=>CHtml::link('Inicio', array('/Admin')),
    'links'=>array(
		'Productos'=>array('admin'),
		'Modificar',
	),
));
?>

<h1>Editar producto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelPhotos'=>$modelPhotos)); ?>